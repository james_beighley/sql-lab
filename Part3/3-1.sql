create or replace FUNCTION get_time()
RETURNS timestamp AS $currenttime$
declare 
currenttime timestamp;
begin
select NOW() into currenttime;
return currenttime;
end;
$currenttime$ LANGUAGE plpgsql;

select get_time();



select * from media_type;



CREATE or replace FUNCTION get_mediatype_length(integer)
RETURNS integer AS $mediatype_length$
declare
currentlength integer;
begin
select length(name) into currentlength from media_type where media_type_id = $1;
return currentlength;
end;
$mediatype_length$ LANGUAGE plpgsql;

select get_mediatype_length(1);
