select * from employee;
DROP FUNCTION getemployeesbornafter1968();
create or replace FUNCTION getemployeesbornafter1968()
RETURNS table(firstname VARCHAR(20), lastname VARCHAR(20)) AS $$
declare
begin
return query
	select first_name,last_name from employee where extract(YEAR FROM birth_date)>1968;
end;
$$ LANGUAGE plpgsql;

select getemployeesbornafter1968();