select * from invoice;
create or replace FUNCTION getAverageTotalInvoices()
RETURNS decimal AS $currenttime$
declare 
currenttime decimal;
begin
select avg(total) into currenttime from invoice;
return currenttime;
end;
$currenttime$ LANGUAGE plpgsql;

select getAverageTotalInvoices();
DROP FUNCTION getmostexpensivetrack();
create or replace FUNCTION getMostExpensiveTrack()
RETURNS table(trackname VARCHAR(200), price NUMERIC(10,2)) AS $$
declare
begin
return query
	select name, unit_price from track where unit_price = (select max(unit_price) from track);
end;
$$ LANGUAGE plpgsql;

select getMostExpensiveTrack();