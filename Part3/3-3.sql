select * from invoice_line; 

create or replace FUNCTION averagepriceinvoicelineitems()
RETURNS decimal AS $currenttime$
declare 
currenttime decimal;
begin
select avg(unit_price) into currenttime from invoice_line;
return currenttime;
end;
$currenttime$ LANGUAGE plpgsql;

select averagepriceinvoicelineitems();